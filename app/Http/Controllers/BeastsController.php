<?php

namespace App\Http\Controllers;

use Validator;
use App\Http\Requests\BeastsRequest;
use App\Http\Requests\ClassesRequest;
use App\Http\Requests\weakPointsRequest;
use App\Http\Controllers\Controller;
use App\Beasts;
use App\Classes;
use App\weakPoints;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BeastsController extends Controller
{
    public function mod()
    {
        if (!(Auth::guest())) {
            return (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()));
        } else {
            return false;
        }
    }
    public function usr()
    {
        if (!(Auth::guest())) {
            return (in_array('User', Auth::user()->types()->pluck('userType')->toArray()));
        } else {
            return false;
        }
    }

    public function addShow()
    {
        if ($this->mod()) {
            return view('addPage', ['weakPoints' => weakPoints::all(), 'classes' => Classes::all()]);
        } else {
            return redirect()->route('home');
        }
    }

    public function addData(BeastsRequest $request)
    {
        if ($this->mod()) {
            $beast = new Beasts;
            $class = new Classes();
            $weakPoint = new weakPoints();

            $beast->authorId = Auth::user()->id;

            $class = Classes::firstOrCreate(['className' => $request->input('className')]);
            $class->save();

            $weakPoint = weakPoints::firstOrCreate(['weakPointName' => $request->input('weakPointName')]);
            $weakPoint->save();

            $beast->name = $request->input('name');
            $beast->description = $request->input('description');

            $class->beasts()->save($beast);
            $weakPoint->beasts()->save($beast);

            $beast->save();

            return redirect()->route('showBeast', ['beast' => Beasts::find($beast->id)]);
        } else {
            return  redirect()->route('home');
        }
    }

    public function showData()
    {
        //Заготовка на будущее, это фильтрация, предлагаю сделать по классам и сабым местам
        //$c = "Животное";
        //$k = Classes::where('className', $c)->get()->pluck('id')[0];
        //$l = Beasts::where('classId', $k);
        //------$l = DB::table('beasts')->join('classes', 'beasts.classId', '=', 'classes.id')->select('beasts.*')->where('classes.className', $c)->get();

        if ($this->usr() or $this->mod()) {
            return view('showList', ['data' => Beasts::paginate(11)]);
        } else {
            return view('showList', ['data' => Beasts::paginate(12)]);
        }
    }

    public function showDataFiltered(Request $request)
    {
        //Это фильтрация, по названию монстра
        if ($request->input('search')) {
            $str = $request->input('search');
            $res = Beasts::where('name', 'like', '%' . $str . '%');
            if ($res) {
                if ($this->usr() or $this->mod()) {
                    return view('showList', ['data' => $res->paginate(11)->appends(request()->query())]);
                } else {
                    return view('showList', ['data' => $res->paginate(12)->appends(request()->query())]);
                }
            }
        } else {
            return redirect()->route('home');
        }
    }

    public function showBeast($id)
    {
        return view('showBeast', ['beast' => Beasts::find($id)]);
    }

    public function updateData($id, BeastsRequest $request)
    {
        if ($this->mod()) {
            $beast = Beasts::find($id);
            $class = new Classes();
            $weakPoint = new weakPoints();

            $class = Classes::firstOrCreate(['className' => $request->input('className')]);
            $class->save();

            $weakPoint = weakPoints::firstOrCreate(['weakPointName' => $request->input('weakPointName')]);
            $weakPoint->save();

            $beast->name = $request->input('name');
            $beast->description = $request->input('description');

            $class->beasts()->save($beast);
            $weakPoint->beasts()->save($beast);

            $beast->save();

            return redirect()->route('showBeast', $beast->id);
        } else {
            return redirect()->route('home');
        }
    }

    public function updateShow($id)
    {
        if ($this->mod()) {
            $beast = new Beasts();
            return view('updatePage', ['beast' => $beast->find($id), 'weakPoints' => weakPoints::all(), 'classes' => Classes::all()]);
        } else {
            return redirect()->route('home');
        }
    }

    public function deleteShow($id)
    {
        if ($this->mod()) {
            return view('deletePage', ['beast' => Beasts::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function deleteData($id)
    {
        if ($this->mod()) {
            Beasts::find($id)->delete();
            return redirect()->route('home');
        } else {
            return redirect()->route('home');
        }
    }


    public function classesShow()
    {
        if ($this->mod()) {
            return view('classesShow', ['classes' => Classes::paginate(10)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function classAddShow()
    {
        if ($this->mod()) {
            return view('classAddShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function classAddSubmit(ClassesRequest $request)
    {
        if ($this->mod()) {
            $class = new Classes();
            $class->className = $request->input('className');
            $class->save();
            return redirect()->route('classesShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function classUpdateShow($id)
    {
        if ($this->mod()) {
            if ($id == 0) {
                return redirect()->route('classesShow');
            }
            return view('classUpdateShow', ['class' => Classes::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function classUpdateSubmit(ClassesRequest $request)
    {
        if ($this->mod()) {
            $class = Classes::find($request->id);
            $class->className = $request->input('className');
            $class->save();
            return redirect()->route('classesShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function classDeleteShow($id)
    {
        if ($this->mod()) {
            if ($id == 0) {
                return redirect()->route('classesShow');
            }
            return view('classDeleteShow', ['class' => Classes::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function classDeleteSubmit($classId)
    {
        if ($this->mod()) {
            $beastsId = Classes::find($classId)->beasts()->pluck('id')->toArray();
            foreach ($beastsId as $id) {
                $beast = Beasts::find($id);
                $beast->classId = 0;
                $beast->save();
            }
            Classes::find($classId)->delete();
            return redirect()->route('classesShow');
        } else {
            return redirect()->route('home');
        }
    }


    public function weakPointsShow()
    {
        if ($this->mod()) {
            return view('weakPointsShow', ['weakPoints' => WeakPoints::paginate(10)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function weakPointsAddShow()
    {
        if ($this->mod()) {
            return view('weakPointsAddShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function weakPointsAddSubmit(weakPointsRequest $request)
    {
        if ($this->mod()) {
            $weakPoint = new WeakPoints();
            $weakPoint->weakPointName = $request->input('weakPointName');
            $weakPoint->save();
            return redirect()->route('weakPointsShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function weakPointsUpdateShow($id)
    {
        if ($this->mod()) {
            if ($id == 0) {
                return redirect()->route('weakPointsShow');
            }
            return view('weakPointsUpdateShow', ['weakPoint' => WeakPoints::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function weakPointsUpdateSubmit(weakPointsRequest $request)
    {
        if ($this->mod()) {
            $weakPoint = WeakPoints::find($request->id);
            $weakPoint->weakPointName = $request->input('weakPointName');
            $weakPoint->save();
            return redirect()->route('weakPointsShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function weakPointsDeleteShow($id)
    {
        if ($this->mod()) {
            if ($id == 0) {
                return redirect()->route('weakPointsShow');
            }
            return view('weakPointsDeleteShow', ['weakPoint' => WeakPoints::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function weakPointsDeleteSubmit($weakPointId)
    {
        if ($this->mod()) {
            $beastsId = WeakPoints::find($weakPointId)->beasts()->pluck('id')->toArray();
            foreach ($beastsId as $id) {
                $beast = Beasts::find($id);
                $beast->weakPointId = 0;
                $beast->save();
            }
            WeakPoints::find($weakPointId)->delete();
            return redirect()->route('weakPointsShow');
        } else {
            return redirect()->route('home');
        }
    }
}
