<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Beasts;
use App\UserType;
use App\Suggestions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class UsersController extends Controller
{
    public function adm()
    {
        if (!(Auth::guest())) {
            return (in_array('Admin', Auth::user()->types()->pluck('userType')->toArray()));
        } else {
            return false;
        }
    }

    public function usr()
    {
        if (!(Auth::guest())) {
            return (in_array('User', Auth::user()->types()->pluck('userType')->toArray()));
        } else {
            return false;
        }
    }
    
    public function mod()
    {
        if (!(Auth::guest())) {
            return (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()));
        } else {
            return false;
        }
    }

    public function usersShow()
    {
        if ($this->adm()) {
            return view('showUsers', ['users' => User::paginate(10)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function usersAddShow()
    {
        if ($this->adm()) {
            return view('addUserPage', ['types' => UserType::all()]);
        } else {
            return redirect()->route('home');
        }
    }

    public function usersAddSubmit(UsersRequest $request)
    {
        if ($this->adm()) {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
            ]);
            if ($request->input('types')) {
                $user->types()->attach($request->input('types'));
            } else {
                $user->types()->attach([3]);
            }
            return redirect()->route('usersShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function userDeleteShow($id)
    {
        if ($this->adm()) {
            if ($id == 0) {
                return redirect()->route('usersShow');
            }
            return view('deleteUserPage', ['user' => User::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function userDeleteSubmit($id)
    {
        if ($this->adm()) {
            User::find($id)->types()->detach();
            User::find($id)->delete();
            return redirect()->route('usersShow');
        } else {
            return redirect()->route('home');
        }
    }

    public function userUpdateShow($id)
    {
        if ($this->adm()) {
            if ($id == 0) {
                return redirect()->route('usersShow');
            }
            return view('updateUserPage', ['user' => User::find($id), 'types' => UserType::get()]);
        } else {
            return redirect()->route('home');
        }
    }

    public function userUpdateSubmit(UsersUpdateRequest $request, $id)
    {
        if ($this->adm()) {
            $user = User::find($id)->update([
                'name' => $request['name'],
                'email' => $request['email']
            ]);
            User::find($id)->types()->detach();
            if ($request->input('types')) {
                User::find($id)->types()->attach($request->input('types'));
            } else {
                User::find($id)->types()->attach([3]);
            }
            return redirect()->route('usersShow');
        } else {
            return redirect()->route('home');
        }
    }
    public function suggestAddShow()
    {
        if ($this->usr()) {
            return view('suggestionsAdd');
        } else {
            return redirect()->route('home');
        }
    }

    public function suggestAddSubmit(Request $request)
    {
        if ($this->usr()) {
            $msg = new Suggestions();
            $msg->description = $request->input('msg');
            $msg->userId = Auth::user()->id;
            $msg->save();
            return redirect()->route('home');
        } else {
            return redirect()->route('home');
        }
    }

    public function suggestListShow()
    {
        if ($this->mod()) {
            return view('suggestionsList', ['msgs' => Suggestions::paginate(10)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function suggestListDelete($id)
    {
        if ($this->mod()) {
            return view('suggestionsListDelete', ['msg' => Suggestions::find($id)]);
        } else {
            return redirect()->route('home');
        }
    }

    public function suggestListDeleteSudmit($id)
    {
        if ($this->mod()) {
            Suggestions::find($id)->delete();
            return view('suggestionsList', ['msgs' => Suggestions::paginate(10)]);
        } else {
            return redirect()->route('home');
        }
    }
}
