<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Такой e-mail уже используется',
            'name.required' => 'Поле Имя обязательно',
            'email.required' => 'Поле E-mail обязательно',
            'password.required' => 'Поля Пароль обязательны',
            'name.max' => 'Поле Название слишком длинное (макс. длина: 255 символов)',
            'email.max' => 'Поле E-mail слишком длинное (макс. длина: 255 символов)',
            'password.min' => 'Поле Пароль слишком короткое (мин. длина: 6 символов)',
            'email.email' => 'Поле E-mail должно иметь вид e-mail',
            'password.confirmed' => 'Пароли не совпадают'
        ];
    }
}
