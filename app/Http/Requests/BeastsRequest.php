<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BeastsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:30',
            'description' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле Название обязательно',
            'description.required' => 'Поле Описание обязательно',
            'name.max' => 'Поле Название слишком длинное (макс. длина: 30 символов)',
        ];
    }
}
