<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'className' => 'required|max:255|unique:classes',
        ];
    }

    public function messages()
    {
        return [
            'className.unique' => 'Такой класс уже существует',
            'className.required' => 'Поле Название обязательно',
            'className.max' => 'Поле Название слишком длинное (макс. длина: 255 символов)',
        ];
    }
}
