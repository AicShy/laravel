<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class weakPointsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'weakPointName' => 'required|max:255|unique:weak_points',
        ];
    }

    public function messages()
    {
        return [
            'weakPointName.unique' => 'Такое слабое место уже существует',
            'weakPointName.required' => 'Поле Название обязательно',
            'weakPointName.max' => 'Поле Название слишком длинное (макс. длина: 255 символов)',
        ];
    }
}
