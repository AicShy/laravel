<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $fillable = ['className'];
    public function beasts()
    {
      return $this->hasMany('App\Beasts', 'classId');
    }
}
