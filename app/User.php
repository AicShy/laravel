<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function types() {
        return $this->belongsToMany('App\UserType', 'userConnection', 'userId', 'userTypeId');
    }

    public function beasts()
    {
      return $this->hasMany('App\Beasts', 'authorId');
    }

    public function suggestions()
    {
      return $this->hasMany('App\Suggestions', 'userId');
    }
}
