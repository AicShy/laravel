<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class weakPoints extends Model
{
    protected $fillable = ['weakPointName'];
    
    public function beasts()
    {
      return $this->hasMany('App\Beasts', 'weakPointId');
    }
}
