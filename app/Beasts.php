<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beasts extends Model
{
    public function class()
    {
      return $this->belongsTo('App\Classes', 'classId');
    }

    public function weakPoint()
    {
      return $this->belongsTo('App\weakPoints', 'weakPointId');
    }

    public function author()
    {
      return $this->belongsTo('App\User', 'authorId');
    }
}
