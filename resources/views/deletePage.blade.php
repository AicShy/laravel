@extends('layouts.app')

@section('title')Удалить запись о {{$beast->name}}@endsection

@section('content')
<div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
	<div class="d-flex flex-column flex-wrap p-3 align-items-center w-100">
		<h3 class="text-center text-danger mb-3">Вы уверены, что хотите удалить запиcь о <br>{{$beast->name}}?</h3>
		<div class="card mb-3" style="width: 17rem;">
			@if($beast->picture)
			<img src="http://pics.std-1226.ist.mospolytech.ru/{{$beast->picture}}" class="card-img" alt="">
			@else
			<img src="http://pics.std-1226.ist.mospolytech.ru/placeholder.png" class="card-img" alt="">
			@endif
			<div class="card-body mb-3">
				<h5 class="card-text">Класс</h5>
				@if($beast->class)
				<p>{{$beast->class->className}}</p>
				@else
				<p>Неизвестен</p>
				@endif
				<h5 class="card-text">Слабое место</h5>
				@if($beast->weakPoint)
				<p>{{$beast->weakPoint->weakPointName}}</p>
				@else
				<p>Неизвестно</p>
				@endif
			</div>
		</div>
		<form method="post" action="{{route('deleteSubmit', $beast->id)}}" class=" w-100">
			{{ csrf_field() }}
			<div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
				<input name="delete" value="Да" type="submit" class="btn btn-danger">
				<input type="button" value="Отмена" class="btn btn-primary" onClick="location.href=`{{route('showBeast', $beast->id)}}`">
			</div>
		</form>
	</div>
</div>
@endsection