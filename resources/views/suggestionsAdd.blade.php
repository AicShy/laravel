@extends('layouts.app')

@section('title')Добавить предложение@endsection

@section('content')

<form method="post" action="{{route('suggestAddSubmit')}}">
    {{ csrf_field() }}
    <div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
        <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
            @include('common.errors')
            <h3 class="text-center mb-3">Тут можно предложить запись</h3>
            <div class="d-flex flex-column flex-wrap">
                <h5 class="mb-1 text-center">Опишите своего монстра</h5>
                <textarea type='text' name='msg' class="mb-3 form-control" style="min-height: 100px;" required></textarea>
                <div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
                    <input name="edit" value="Отправить" type="submit" class="btn btn-success">
                    <input type="button" value="Отмена" class="btn btn-danger" onClick="location.href=`{{route('home')}}`">
                </div>
            </div>
        </div>
    </div>
</form>
@endsection