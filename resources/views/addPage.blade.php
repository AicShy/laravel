@extends('layouts.app')

@section('title')Добавить монстра@endsection

@section('content')

<form method="post" action="{{route('addSubmit')}}">
    {{ csrf_field() }}

    <div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
        <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
            @include('common.errors')
            <h3 class="text-center  mb-3">Тут можно добавить запись</h3>
            <div class="d-flex flex-wrap justify-content-center col-12">
                <h5 class="mb-1 text-center">Название</h5>
                <input type='text' name='name' class="mb-3 form-control" value="{{ old('name') }}">
            </div>
            <div class="d-flex flex-wrap justify-content-between w-100 mb-2">
                <div class="d-flex flex-wrap justify-content-center col-6">
                    <h5 class="mb-1 text-center w-100">Класс</h5>
                    </h5>
                    <select class="custom-select w-100" name='className'>
                        @foreach($classes as $class)
                        <option @if ($class->className == old('className'))
                            selected
                            @endif>
                            {{$class->className}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="d-flex flex-wrap justify-content-center col-6">
                    <h5 class="mb-1 text-center">Слабое место</h5>
                    </h5>
                    <select class="custom-select" name='weakPointName'>
                        @foreach($weakPoints as $weakPoint)
                        <option @if ($weakPoint->weakPointName == old('weakPointName'))
                            selected
                            @endif>
                            {{$weakPoint->weakPointName}}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="d-flex flex-wrap justify-content-center col-12">
                <h5 class="mb-1 text-center">Описание</h5>
                <textarea type='text' name='description' class="mb-3 form-control" style="min-height: 125px;">{{ old('description') }}</textarea>
            </div>
            <div class="d-flex flex-row flex-wrap justify-content-around align-items-center w-100">
                <input name="edit" value="Добавить" type="submit" class="btn btn-success">
                <input type="button" value="Отмена" class="btn btn-danger" onClick="location.href=`{{route('home')}}`">
            </div>
        </div>
    </div>
</form>
@endsection