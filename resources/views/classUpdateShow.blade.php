@extends('layouts.app')

@section('title')Обновить класс "{{$class->className}}"@endsection

@section('content')

<form method="post" action="{{route('classUpdateSubmit', $class->id)}}">

    {{ csrf_field() }}
    <div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
        <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
            @include('common.errors')
            <h3 class="text-center mb-3">Тут можно изменить запись о классе <br>{{$class->className}}</h3>

            <div class="d-flex flex-column flex-wrap">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} d-flex flex-column flex-wrap">
                    <h5 for="name" class="text-center mb-3">Название</h5>
                    <div>
                        <input id="name" type="text" class="form-control w-100" name="className" value="{{$class->className}}" required autofocus>
                    </div>
                </div>

                <div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
                    <input value="Сохранить" type="submit" class="btn btn-success mr-4">
                    <input value="Отмена" type="button" class="btn btn-danger" onClick="location.href=`{{route('classesShow')}}`">
                </div>
            </div>
        </div>
    </div>

</form>
@endsection