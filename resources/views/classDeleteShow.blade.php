@extends('layouts.app')

@section('title')Удалить класс "{{$class->className}}"@endsection

@section('content')
<div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
    <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
        <h3 class="text-center text-danger mb-3">Вы уверены, что хотите удалить запиcь о <br> {{$class->className}}?</h3>
        <table class="table mb-3">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Класс</th>
                    <th scope="col">Представители класса</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$class->id}}</td>
                    <td>{{$class->className}}</td>
                    <td>
                        @if($class->beasts()->pluck('name')->implode(', '))
                        {{$class->beasts()->pluck('name')->implode(', ')}}
                        @else
                        Нет
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
        <form method="post" action="{{route('classDeleteSubmit', $class->id)}}">
            {{ csrf_field() }}
            <div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
                <input name="delete" value="Да" type="submit" class="btn btn-danger">
                <input type="button" value="Отмена" class="btn btn-primary" onClick="location.href=`{{route('classesShow')}}`">
            </div>
        </form>
    </div>
</div>
@endsection