@extends('layouts.app')

@section('title')Удалить пользователя {{$user->name}}@endsection

@section('content')
<div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
    <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
        <h3 class="text-center text-danger mb-3">Вы уверены, что хотите удалить пользователя <br>{{$user->name}}?</h3>
        <div class="w-100" style="overflow-x: auto;">
            <table class="table mb-3">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Пользователь</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Роли</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->types()->pluck('userType')->implode(', ')}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <form method="post" action="{{route('userDeleteSubmit', $user->id)}}">
            {{ csrf_field() }}
            <div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
                <input name="delete" value="Да" type="submit" class="btn btn-danger">
                <input type="button" value="Отмена" class="btn btn-primary" onClick="location.href=`{{route('usersShow')}}`">
            </div>
        </form>
    </div>
</div>
@endsection