<style>
    .page-item.active .page-link {
        background-color: #F9F9E3;
        border-color: #302D2D;
        color: black;
    }

    .page-link {
        background-color: #161515;
        border-color: #161515;
        color: white;
    }

    .page-item.disabled .page-link {
        background-color: #161515;
        border-color: #161515;
        color: grey;
    }

    .page-item.active .page-link:hover {
        background-color: #F1F1D6;
        border-color: #302D2D;
        color: black;
    }

    .page-link:hover {
        background-color: #302D2D;
        border-color: #302D2D;
        color: white;
    }
</style>
@if ($paginator->lastPage() > 1)
<ul class="pagination">
    <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }} page-item">
        <a class="page-link" href="{{ $paginator->url(1) }}">Начало</a>
    </li>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }} page-item">
            <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }} page-item">
        <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" >Конец</a>
    </li>
</ul>
@endif