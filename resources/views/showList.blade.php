@extends('layouts.app')

@section('title')Главная@endsection

@section('content')

@if (!(Auth::guest()))
@if (Auth::user()->name == 'jebaited')
<script>
    window.location.href = '{{route("jebaited")}}';
</script>
@endif
@endif

<style>
    .card-img-overlay {
        background: linear-gradient(180deg, rgba(0, 0, 0, 0) 65%, #000000 100%);
    }

    .card {
        height: 340px;
        overflow: hidden;
        background-color: black;
        border-color: black !important;
    }

    .card:hover {
        box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);
        -webkit-transform: scale(1.01);
        -ms-transform: scale(1.01);
        transform: scale(1.01);
    }

    .no-result {
        height: 340px;
    }
</style>

<div class="d-flex flex-row flex-wrap py-3 rounded" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);">
    <div class="d-flex flex-row justify-content-end w-100 mx-3 ">
        <nav id="pag" aria-label="Page navigation example">
            @include('pagination', ['paginator' => $data])
        </nav>
    </div>
    @if (!Auth::guest())
    @if (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()))
    <div class="col-md-3 col-sm-6 mb-3">
        <div class="card text-white d-flex justify-content-center" onClick="location.href=`{{route('add')}}`">
            <img src="http://pics.std-1226.ist.mospolytech.ru/add.png" class="card-img" alt="">
            <div class="card-img-overlay d-flex flex-column align-items-center justify-content-end">
                <h5 class="card-title text-center">Добавить монстра</h5>
            </div>
        </div>
    </div>
    @endif

    @if ((in_array('User', Auth::user()->types()->pluck('userType')->toArray())) and !(in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray())))
    <div class="col-md-3 col-sm-6 mb-3">
        <div class="card text-white d-flex justify-content-center border border-light" onClick="location.href=`{{route('suggest')}}`">
            <img src="http://pics.std-1226.ist.mospolytech.ru/add.png" class="card-img" alt="">
            <div class="card-img-overlay d-flex flex-column align-items-center justify-content-end">
                <h5 class="card-title text-center">Предложить монстра</h5>
            </div>
        </div>
    </div>
    @endif
    @endif
    @if($data[0])
    @foreach($data as $beast)
    <div class="col-md-3 col-sm-6 mb-3">
        <div class="card text-white d-flex justify-content-center border border-light" onClick="location.href=`{{route('showBeast', $beast->id)}}`">
            @if($beast->picture)
            <img src="http://pics.std-1226.ist.mospolytech.ru/{{$beast->picture}}" class="card-img" alt="">
            @else
            <img src="http://pics.std-1226.ist.mospolytech.ru/placeholder.png" class="card-img" alt="">
            @endif
            <div class="card-img-overlay d-flex flex-column align-items-center justify-content-end">
                <h5 class="card-title text-center">{{$beast->name}}</h5>
            </div>
        </div>
    </div>
    @endforeach
    @else
    @if (Auth::guest())
    <div class="d-flex flex-column align-items-center justify-content-center no-result w-100">
        <h3 class="card-title">Ничего не найдено</h3>
    </div>
    @endif
    @endif
    <div class="d-flex flex-row justify-content-end w-100 mx-3">
        <nav id="pag" aria-label="Page navigation example">
            @include('pagination', ['paginator' => $data])
        </nav>
    </div>
</div>
@endsection