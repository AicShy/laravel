<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="http://pics.std-1226.ist.mospolytech.ru/fav.ico" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        .btn:focus,
        .btn:active {
            outline: none !important;
            box-shadow: none;
        }

        .form-control:focus,
        .form-control:active {
            outline: none !important;
            box-shadow: none;
        }

        /* @media (min-width: 768px) {
            .navbar-expand-lg .navbar-collapse {
                display: flex !important;
                flex-basis: auto;
            }
        } */

        a {
            color: black;
        }

        a:hover {
            color: black;
        }

        .form-control-custom {
            padding-right: 29px;
            z-index: 0;
        }

        .search-btn {
            height: calc(1.5em + .75rem + 2px);
            float: left;
            background: white;
            color: black;
            border: 1px solid #ced4da;
            margin-left: -29px;
            border-top-right-radius: .25rem;
            border-bottom-right-radius: .25rem;
            z-index: 1;
        }

        nav.navbar {
            background-color: #F9F9E3;
            box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.9);
        }

        footer {
            height: 100px;
            background-color: #F9F9E3;
            box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.9);
            width: 100%
        }

        tr {
            background: #F9F9E3;
        }

        th,
        tr:nth-child(2n) {
            background: #ECECC8;
        }

        td,
        th {
            border: none !important;
        }

        .card {
            border: none;
        }

        a {
            text-decoration: none;
        }

        a:hover {
            text-decoration: none;
        }
    </style>
</head>

<body style="background-color: #382D2D;">
    <div id="app" class=" d-flex flex-column justify-content-between h-100">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-xl">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <form class="form-inline my-2 my-lg-0 mr-auto" method="get" action="{{route('showDataFiltered')}}">
                {{ csrf_field() }}
                    <div class="d-flex align-items-center">
                        <input class="form-control form-control-custom" type="search" placeholder="Поиск среди бестий" aria-label="Search" name="search">
                        <button class="my-2 my-sm-0 search-btn d-flex align-items-center" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
                                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
                            </svg></button>
                    </div>
                </form>

                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav navbar-right ml-auto">
                        @if (Auth::guest())
                        <li class="mr-3"><a href="{{ url('/login') }}">Войти</a></li>
                        <li><a href="{{ url('/register') }}">Зарегистрироваться</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="nav-link text-dark dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                @if (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()))
                                <a class="dropdown-item" onClick="location.href=`{{route('classesShow')}}`">Управление
                                    классами</a>
                                @endif
                                @if (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()))
                                <a class="dropdown-item" onClick="location.href=`{{route('weakPointsShow')}}`">Управление слабыми местами</a>
                                @endif
                                @if (in_array('Admin', Auth::user()->types()->pluck('userType')->toArray()))
                                <a class="dropdown-item" onClick="location.href=`{{route('usersShow')}}`">Управление
                                    пользователями</a>
                                @endif
                                @if (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()))
                                <a class="dropdown-item" onClick="location.href=`{{route('suggestListShow')}}`">Управление предложениями</a>
                                @endif
                                @if ((in_array('Admin', Auth::user()->types()->pluck('userType')->toArray())) or
                                (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray())))
                                <div class="dropdown-divider"></div>
                                @endif
                                <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-xl d-flex flex-column flex-wrap flex-row justify-content-start align-items-center pb-3" style="min-height: 79vh;">
            <a href="{{route('home')}}">
                <h1 class="text-center mt-3 mb-3" style="color:white">Бестиарий</h1>
            </a>
            <div class="w-100">
            @yield('content')
            </div>
        </div>
    </div>
    <footer class="text-muted d-flex flex-column justify-content-center footer">
        <div class="container">
            <p class="float-right">
                <a href="#">Вернуться наверх</a>
            </p>
            Это как бы футер, которые я не умею писать...
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
    <!-- <script src="/js/app.js"></script> -->
</body>

</html>