@extends('layouts.app')

@section('title')Обновить запись о {{$beast->name}}@endsection

@section('content')

<form method="post" action="{{route('updateSubmit', $beast->id)}}">

    {{ csrf_field() }}

    <div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
        <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
            @include('common.errors')
            <h3 class="text-center mb-3">Тут можно изменить запись о монстре {{$beast->name}}</h3>
            <p class="text-muted text-center">(id записи - {{$beast->id}}) </p>
            <div class="d-flex flex-wrap justify-content-center col-12">
                <h5 class="mb-1 text-center">Название</h5>
                <input type='text' name='name' class="mb-3 form-control" value="{{$beast->name}}">
            </div>
            <div class="d-flex flex-wrap justify-content-between w-100 mb-2">
                <div class="d-flex flex-wrap justify-content-center col-6">
                    <h5 class="mb-1 text-center">Класс</h5>
                    </h5>
                    <select class="custom-select" name='className'>
                        @foreach($classes as $class)
                        <option @if ($class->className == $beast->class->className)
                            selected
                            @endif>
                            {{$class->className}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="d-flex flex-wrap justify-content-center col-6">
                    <h5 class="mb-1 text-center">Слабое место</h5>
                    </h5>
                    <select class="custom-select" name='weakPointName'>
                        @foreach($weakPoints as $weakPoint)
                        <option @if ($weakPoint->weakPointName == $beast->weakPoint->weakPointName)
                            selected
                            @endif>
                            {{$weakPoint->weakPointName}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="d-flex flex-wrap justify-content-center col-12">
                    <h5 class="mb-1 text-center">Описание</h5>
                    <textarea type='text' name='description' class="mb-3 form-control" style="min-height: 125px;">{{$beast->description}}</textarea>
                </div>
                <div class="d-flex flex-row flex-wrap justify-content-around align-items-center w-100">
                    <input name="edit" value="Сохранить" type="submit" class="btn btn-success">
                    <input type="button" value="Отмена" class="btn btn-danger" onClick="location.href=`{{route('showBeast', $beast->id)}}`">
                </div>
            </div>
        </div>
    </div>
</form>
@endsection