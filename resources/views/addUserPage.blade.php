@extends('layouts.app')

@section('title')Добавить пользователя@endsection

@section('content')

<form method="post" action="{{route('usersAddSubmit')}}">
    {{ csrf_field() }}
    <div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
        <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
            @include('common.errors')
            <h3 class="text-center mb-3">Тут можно добавить пользователя</h3>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} d-flex flex-column flex-wrap">
                <h5 for="name" class="text-center mb-3">Имя</h5>
                <div>
                    <input id="name" type="text" class="form-control w-100" name="name" value="{{ old('name') }}" autofocus>
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} d-flex flex-column flex-wrap">
                <h5 for="email" class="text-center mb-3">E-Mail</h5>

                <div>
                    <input id="email" type="email" class="form-control w-100" name="email" value="{{ old('email') }}">
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} d-flex flex-column flex-wrap">
                <h5 for="password" class="text-center mb-3">Пароль</h5>

                <div>
                    <input id="password" type="password" class="form-control w-100" name="password">
                </div>
            </div>

            <div class="form-group d-flex flex-column flex-wrap">
                <h5 for="password-confirm" class="text-center mb-3">Подтвердите пароль</h5>

                <div>
                    <input id="password-confirm" type="password" class="form-control w-100" name="password_confirmation">
                </div>
            </div>

            <div class="form-group d-flex flex-column flex-wrap mb-4">
                <h5 class="text-center mb-3">Роли</h5>
                <div class="d-flex flex-wrap justify-content-around align-items-center mb-3">
                    @foreach ($types as $type)
                    <div class="d-flex align-items-center mr-3">
                        <input type="checkbox" name="types[]" value="{{$type->id}}" class="mr-2">
                        <label class="mb-0">{{ ($type->userType)}}</label>
                    </div>
                    @endforeach
                </div>

                <div class="d-flex flex-row justify-content-around align-items-center">
                    <input value="Добавить" type="submit" class="btn btn-success mr-4">
                    <input type="button" value="Отмена" class="btn btn-danger" onClick="location.href=`{{route('usersShow')}}`">
                </div>
            </div>
        </div>
    </div>

</form>
@endsection