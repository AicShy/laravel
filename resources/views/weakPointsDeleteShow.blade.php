@extends('layouts.app')

@section('title')Удалить слабое место "{{$weakPoint->weakPointName}}"@endsection

@section('content')
<div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
    <div class="d-flex flex-column flex-wrap col-md-9 col-sm-12 p-3">
        <h3 class="text-center text-danger mb-3">Вы уверены, что хотите удалить запиcь о <br> {{$weakPoint->weakPointName}}?</h3>
        <div class="w-100" style="overflow-x: auto;">
            <table class="table mb-3">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Класс</th>
                        <th scope="col">Представители класса</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$weakPoint->id}}</td>
                        <td>{{$weakPoint->weakPointName}}</td>
                        <td>
                            @if($weakPoint->beasts()->pluck('name')->implode(', '))
                            {{$weakPoint->beasts()->pluck('name')->implode(', ')}}
                            @else
                            Нет
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <form method="post" action="{{route('weakPointsDeleteSubmit', $weakPoint->id)}}">
            {{ csrf_field() }}
            <div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
                <input name="delete" value="Да" type="submit" class="btn btn-danger">
                <input type="button" value="Отмена" class="btn btn-primary" onClick="location.href=`{{route('weakPointsShow')}}`">
            </div>
        </form>
    </div>
</div>
@endsection