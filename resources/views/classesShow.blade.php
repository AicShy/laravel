@extends('layouts.app')

@section('title')Классы@endsection

@section('content')

<div class="d-flex flex-row justify-content-center flex-wrap py-3 rounded" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);">
    <h3 class="text-center mb-3">Список классов</h3>
    <div class="d-flex flex-row flex-wrap justify-content-end w-100 mx-3">
        <div class="mb-3">
            <input type="button" value="Добавить запись" class="btn btn-success mb-2" onClick="location.href=`{{route('classAddShow')}}`">
        </div>
        <nav id="pag" aria-label="Page navigation example" class="ml-auto mb-2">
            @include('pagination', ['paginator' => $classes])
        </nav>
    </div>
    <div class="w-100" style="overflow-x: auto;">
        <table class="table mb-3">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Класс</th>
                    <th scope="col">Представители класса</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($classes as $class)
                <tr>
                    <td><b>{{$class->id}}</b></td>
                    <td>{{$class->className}}</td>
                    <td>
                        @if($class->beasts()->pluck('name')->implode(', '))
                        {{$class->beasts()->pluck('name')->implode(', ')}}
                        @else
                        Нет
                        @endif
                    </td>
                    <td>@if ($class->id != 0)<input type="button" value="Изменить" class="btn btn-info" onClick="location.href=`{{route('classUpdateShow', $class->id)}}`">@endif</td>
                    <td>@if ($class->id != 0)<input type="button" value="Удалить" class="btn btn-danger" onClick="location.href=`{{route('classDeleteShow', $class->id)}}`">@endif</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="d-flex flex-row justify-content-end w-100 mx-3 ">
        <nav id="pag" aria-label="Page navigation example">
            @include('pagination', ['paginator' => $classes])
        </nav>
    </div>
</div>
@endsection