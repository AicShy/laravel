@extends('layouts.app')

@section('title'){{$beast->name}}@endsection

@section('content')

<div class="d-flex flex-row flex-md-nowrap flex-wrap justify-content-md-between justify-content-sm-center mb-3 p-4 rounded" style="background-color: #F9F9E3">
    <div class="mr-0 mr-md-3 d-flex flex-column justify-content-between flex-grow-1">
        <div>
            <div class="d-flex flex-row align-items-center mb-3">
                <h2 class="mr-3">{{$beast->name}}</h2>
                @if (!Auth::guest())
                @if (in_array('Moderator', Auth::user()->types()->pluck('userType')->toArray()))
                <a>
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-pencil-square mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg" onClick="location.href=`{{route('updateShow', $beast->id)}}`">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                    </svg>
                </a>
                <a>
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg" onClick="location.href=`{{route('deleteShow', $beast->id)}}`">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                    </svg>
                </a>@endif @endif
            </div>
            <h3>Описание</h3>
            <hr class="w-100">
            <p>{{$beast->description}}</p>
        </div>
        <div class="d-flex flex-row justify-content-start w-100">
            <p class="text-muted">Автор -&nbsp;</p>
            @if($beast->author)
            <p class="text-muted">{{$beast->author->name}}</p>
            @else
            <p class="text-muted">Неизвестен</p>
            @endif
        </div>
    </div>
    <div class="d-flex flex-row justify-content-center">
        <div class="card" style="width: 17rem;">
            @if($beast->picture)
            <img src="http://pics.std-1226.ist.mospolytech.ru/{{$beast->picture}}" class="card-img" alt="">
            @else
            <img src="http://pics.std-1226.ist.mospolytech.ru/placeholder.png" class="card-img" alt="">
            @endif
            <div class="card-body">
                <h5 class="card-text">Класс</h5>
                <p>
                    @if($beast->class)
                    {{$beast->class->className}}
                    @else
                    Неизвестен
                    @endif
                </p>
                <h5 class="card-text">Слабое место</h5>
                <p class="mb-0">
                    @if($beast->weakPoint)
                    {{$beast->weakPoint->weakPointName}}
                    @else
                    Неизвестно
                    @endif
                </p>
            </div>
        </div>
    </div>
</div>
@endsection