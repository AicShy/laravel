@extends('layouts.app')

@section('title')Предложения о создании монстров@endsection

@section('content')
<div class="d-flex flex-row justify-content-center flex-wrap py-3 rounded" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);">
    <h3 class="text-center mb-3">Список предложений</h3>
    <div class="d-flex flex-row justify-content-end w-100 mx-3">
        <nav id="pag" aria-label="Page navigation example">
            @include('pagination', ['paginator' => $msgs])
        </nav>
    </div>
    <div style="overflow-x: auto;" class="w-100">
        <table class="table mb-3">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Автор</th>
                    <th scope="col">Предложение</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($msgs as $msg)
                <tr>
                    <td>{{$msg->id}}</td>
                    <td>{{$msg->author->name}}</td>
                    <td>{{$msg->description}}</td>
                    @if (!Auth::guest())<td><input type="button" value="Удалить" class="btn btn-danger" onClick="location.href=`{{route('suggestListDelete', $msg->id)}}`"></td>@endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex flex-row justify-content-end w-100 mx-3 ">
        <nav id="pag" aria-label="Page navigation example">
            @include('pagination', ['paginator' => $msgs])
        </nav>
    </div>
</div>
@endsection