@extends('layouts.app')

@section('title')Пользователи@endsection

@section('content')
<div class="d-flex flex-row justify-content-center flex-wrap py-3 rounded" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);">
    <h3 class="text-center mb-3">Список пользователей</h3>
    <div class="d-flex flex-row flex-wrap justify-content-end w-100 mx-3">
        <div class="mb-2">
            <input type="button" value="Добавить пользователя" class="btn btn-success mb-3" onClick="location.href=`{{route('usersAddShow')}}`">
        </div>
        <nav id="pag" aria-label="Page navigation example" class="ml-auto mb-2">
            @include('pagination', ['paginator' => $users])
        </nav>
    </div>
    <div style="overflow-x: auto;" class="w-100">
        <table class="table mb-3">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Пользователь</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Роли</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->types()->pluck('userType')->implode(', ')}}</td>
                <td>@if ($user->id != 0)<input type="button" value="Изменить" class="btn btn-info" onClick="location.href=`{{route('userUpdateShow', $user->id)}}`">@endif</td>
                <td>@if ($user->id != 0)<input type="button" value="Удалить" class="btn btn-danger" onClick="location.href=`{{route('userDeleteShow', $user->id)}}`">@endif</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="d-flex flex-row justify-content-end w-100 mx-3 ">
        <nav id="pag" aria-label="Page navigation example">
            @include('pagination', ['paginator' => $users])
        </nav>
    </div>
</div>
@endsection