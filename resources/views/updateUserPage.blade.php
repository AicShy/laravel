@extends('layouts.app')

@section('title')Обновить пользователя {{$user->name}}@endsection

@section('content')

<form method="post" action="{{route('userUpdateSubmit', $user->id)}}">

    {{ csrf_field() }}
    <div class="d-flex flex-row rounded justify-content-center col-md-9 col-sm-12 m-auto" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.5);">
        <div class="d-flex flex-column flex-wrap p-3 col-md-9 col-sm-12">
            @include('common.errors')
            <h3 class="text-center mb-3">Тут можно изменить запись о пользователе <br> {{$user->name}}</h3>
            <div class="d-flex flex-column flex-wrap">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} d-flex flex-column flex-wrap">
                    <h5 for="name" class="text-center mb-3">Имя</h5>
                    <div>
                        <input id="name" type="text" class="form-control w-100" name="name" value="{{$user->name}}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} d-flex flex-column flex-wrap">
                    <h5 for="email" class="text-center mb-3">E-Mail</h5>

                    <div>
                        <input id="email" type="email" class="form-control w-100" name="email" value="{{$user->email}}" required>
                    </div>
                </div>

                <div class="form-group d-flex flex-column flex-wrap">
                    <h5 class="text-center mb-3">Роли</h5>
                    <div class="d-flex flex-wrap justify-content-around align-items-center mb-3">
                        @foreach ($types as $type)
                        <div class="d-flex align-items-center mr-3">
                            <input class="mr-2" type="checkbox" name="types[]" value="{{$type->id}}" @if($user->types->where('id', $type->id)->count())
                            checked="checked"
                            @endif
                            >
                            <label class="mb-0">{{ ($type->userType)}}</label>
                        </div>
                        @endforeach
                    </div>

                    <div class="d-flex flex-row flex-wrap justify-content-around align-items-center">
                        <input value="Сохранить" type="submit" class="btn btn-success mr-4">
                        <input value="Отмена" type="button" class="btn btn-danger" onClick="location.href=`{{route('usersShow')}}`">
                    </div>
                </div>
            </div>
        </div>
</form>
@endsection