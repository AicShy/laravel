@extends('layouts.app')

@section('title')Вход@endsection

@section('content')
<div class="d-flex justify-content-center">
    <div class="d-flex align-items-center flex-column col-md-9 col-sm-12 p-3 rounded" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);">
        <h4 class="">Войти</h4>
        <form class="col-md-9 col-sm-12 d-flex flex-column flex-grow-1 justify-content-end" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail</label>

                <div class=" d-flex flex-column">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <p class="text-danger mb-0">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Пароль</label>

                <div class="">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Запомнить меня
                        </label>
                    </div>
                </div>
            </div>

            <div class="">
                <div class=" d-flex flex-column align-items-start">
                    <button type="submit" class="btn btn-primary mb-2">
                        Войти
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection