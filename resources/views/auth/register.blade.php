@extends('layouts.app')

@section('title')Регистрация@endsection

@section('content')
<div class="d-flex justify-content-center">
    <div class="d-flex align-items-center flex-column col-md-9 col-sm-12 p-3 rounded" style="background-color: #F9F9E3; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.9);">
        <form class="col-md-9 col-sm-12 d-flex flex-column flex-grow-1 justify-content-end" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
            <h4 class="text-center">Зарегистрироваться</h4>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Имя</label>

                <div class="d-flex flex-column">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <p class="text-danger mb-0">{{ $errors->first('name') }}</p>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class=" control-label">E-Mail</label>

                <div class="d-flex flex-column">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                    <p class="text-danger mb-0">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Пароль</label>

                <div class="d-flex flex-column">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                    <p class="text-danger mb-0">{{ $errors->first('password') }}</p>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="control-label">Подтвердите пароль</label>

                <div class="d-flex flex-column">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>

            <div class="">
                <div class=" d-flex flex-column align-items-start">
                    <button type="submit" class="btn btn-primary">
                        Зарегистрироваться
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection