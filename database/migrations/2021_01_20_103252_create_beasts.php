<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeasts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beasts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('classId')->unsigned()->nullable();
            $table->foreign('classId')->references('id')->on('classes');
            $table->integer('weakPointId')->unsigned()->nullable();
            $table->foreign('weakPointId')->references('id')->on('weak_points');
            $table->integer('authorId')->unsigned()->nullable();
            $table->foreign('authorId')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beasts');
    }
}