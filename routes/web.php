<?php

use App\Beasts;
use Illuminate\Http\Request;

Route::get('/', 'BeastsController@showData')->name('home');
Route::get('/searchRes', 'BeastsController@showDataFiltered')->name('showDataFiltered');

Route::get('/beast/add', 'BeastsController@addShow')->name('add');
Route::post('/beast/add/submit', 'BeastsController@addData')->name('addSubmit');

Route::get('/beast/{id}', 'BeastsController@showBeast')->name('showBeast');

Route::get('/beast/{id}/update', 'BeastsController@updateShow')->name('updateShow');
Route::post('/beast/{id}/update/submit', 'BeastsController@updateData')->name('updateSubmit');

Route::get('/beast/{id}/delete', 'BeastsController@deleteShow')->name('deleteShow');
Route::post('/beast/{id}/delete/submit', 'BeastsController@deleteData')->name('deleteSubmit');


Route::get('/classes', 'BeastsController@classesShow')->name('classesShow');

Route::get('/classes/add', 'BeastsController@classAddShow')->name('classAddShow');
Route::post('/classes/add/submit', 'BeastsController@classAddSubmit')->name('classAddSubmit');

Route::get('/classes/{id}/delete', 'BeastsController@classDeleteShow')->name('classDeleteShow');
Route::post('/classes/{id}/delete/submit', 'BeastsController@classDeleteSubmit')->name('classDeleteSubmit');

Route::get('/classes/{id}/update', 'BeastsController@classUpdateShow')->name('classUpdateShow');
Route::post('/classes/{id}/update/submit', 'BeastsController@classUpdateSubmit')->name('classUpdateSubmit');


Route::get('/weakPoints', 'BeastsController@weakPointsShow')->name('weakPointsShow');

Route::get('/weakPoints/add', 'BeastsController@weakPointsAddShow')->name('weakPointsAddShow');
Route::post('/weakPoints/add/submit', 'BeastsController@weakPointsAddSubmit')->name('weakPointsAddSubmit');

Route::get('/weakPoints/{id}/delete', 'BeastsController@weakPointsDeleteShow')->name('weakPointsDeleteShow');
Route::post('/weakPoints/{id}/delete/submit', 'BeastsController@weakPointsDeleteSubmit')->name('weakPointsDeleteSubmit');

Route::get('/weakPoints/{id}/update', 'BeastsController@weakPointsUpdateShow')->name('weakPointsUpdateShow');
Route::post('/weakPoints/{id}/update/submit', 'BeastsController@weakPointsUpdateSubmit')->name('weakPointsUpdateSubmit');


Route::get('/users', 'UsersController@usersShow')->name('usersShow');
//Route::get('/users/{id}', 'UsersController@deleteShow')->name('userShow');

Route::get('/users/add', 'UsersController@usersAddShow')->name('usersAddShow');
Route::post('/users/add/submit', 'UsersController@usersAddSubmit')->name('usersAddSubmit');

Route::get('/users/{id}/delete', 'UsersController@userDeleteShow')->name('userDeleteShow');
Route::post('/users/{id}/delete/submit', 'UsersController@userDeleteSubmit')->name('userDeleteSubmit');

Route::get('/users/{id}/update', 'UsersController@userUpdateShow')->name('userUpdateShow');
Route::post('/users/{id}/update/submit', 'UsersController@userUpdateSubmit')->name('userUpdateSubmit');



Route::get('/suggest', 'UsersController@suggestAddShow')->name('suggest');
Route::post('/suggest/submit', 'UsersController@suggestAddSubmit')->name('suggestAddSubmit');

Route::get('/suggestions', 'UsersController@suggestListShow')->name('suggestListShow');
Route::get('/suggestions/{id}', 'UsersController@suggestListDelete')->name('suggestListDelete');
Route::post('/suggestions/{id}/submit', 'UsersController@suggestListDeleteSudmit')->name('suggestListDeleteSudmit');

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/jebaited', function () {
  if (!(Auth::guest())) {
    if (Auth::user()->name == 'jebaited') {
      return view('jebaited');
    }
    else{
      return redirect()->route('home');
    }
  }
  else{
    return redirect()->route('home');
  }
})->name('jebaited');
